FROM python:alpine3.17

WORKDIR /dodo

ADD . /dodo

EXPOSE 8000

CMD ["python", "server.py"]
